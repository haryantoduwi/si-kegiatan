<div class="row">
	<div class="col-sm-12">
		<div class="box box-info">
			 <div class="box-header with-border">
			 	<h3 class="box-title"><?=ucwords($global->headline)?></h3>
			 </div>
			 <div class="box-body">
			 	<!--JIKA DATA ADA DI DATABASE URL BERUBAH KE EDIT-->
			 	<form action="<?= $data? base_url($global->url.'update'):base_url($global->url)?>" method="POST" enctype="multipart/form-data">
			 		<div class="row">
			 			<div class="col-sm-6">
					 		<div class="form-group">
					 			<label>Email</label>
					 			<input type="text" readonly class="form-control" value="<?=$this->session->userdata('email')?>">
					 			<p class="help-block">Kontak admin untuk merubahnya</p>
					 		</div>			 			
					 		<div class="form-group">
					 			<label>Nama lengkap</label>
					 			<input type="text" name="datadiri_nama" class="form-control" value="<?= $data? ucwords($data->datadiri_nama):''?>">
					 		</div>
					 		<div class="form-group">
					 			<label>No. Telp</label>
					 			<input type="text" name="datadiri_notelp" class="form-control" value="<?= $data? $data->datadiri_notelp:''?>">
					 		</div>
					 		<div class="form-group">
					 			<label>Alamat</label>
					 			<input type="text" name="datadiri_alamat" class="form-control" value="<?= $data? ucwords($data->datadiri_alamat):''?>">
					 		</div>
					 		<div class="form-group">
					 			<label>Keterangan</label>
					 			<textarea class="form-control" rows="3" name="datadiri_keterangan"><?= $data? ucwords($data->datadiri_keterangan):''?></textarea>
					 		</div>
			 			</div>
			 			<div class="col-sm-6">
					 		<div class="form-group">
					 			<label>Tanggal Lahir</label>
					 			<input type="text" name="datadiri_tgllahir" class="datepicker form-control" value="<?= $data? date('d-m-Y',strtotime($data->datadiri_tgllahir)):''?>">
					 		</div>			 			
			 				<div class="form-group">
			 					<label>Foto</label>
			 					<input type="file" name="fileupload">
			 					<p class="help-block">Max ukuran 2mb, format JPG|JPEG</p>
			 				</div>
			 			</div>
			 		</div>
			 		<div class="row">
			 			<div class="col-sm-12">
			 				<button type="submit" name="submit" value="submit" class="btn btn-info btn-flat btn-block"><?= $data? 'Update':'Simpan'?></button>
			 			</div>
			 		</div>
			 	</form>
			 </div>
		</div>
	</div>
</div>