<div id="dashboard">
<?php if($data):?>
	<div class="row">
	<div class="col-sm-4">
		<div class="box box-primary">
		<div class="box-body box-profile">
		  <img class="profile-user-img img-responsive img-circle" src="<?= base_url().'/filefoto/'.$data->datadiri_foto?>" alt="Foto Profil" style="width:120px;height:120px">

		  <h3 class="profile-username text-center"><?= ucwords($data->datadiri_nama)?></h3>


		  <ul class="list-group list-group-unbordered">
		    <li class="list-group-item">
		      <b>Email</b><br> <p><?= $this->session->userdata('email')?></p>
		    </li>		  
		    <li class="list-group-item">
		      <b>No.Tlp</b><br> <p><?= ucwords($data->datadiri_notelp)?></p>
		    </li>		  
		    <li class="list-group-item">
		      <b>Tgl.Lahir</b><br> <p><?= date('d-m-Y',strtotime($data->datadiri_tgllahir))?></p>
		    </li>
		    <li class="list-group-item">
		      <b>Alamat</b><br> <p><?= ucwords($data->datadiri_alamat)?></p>
		    </li>	
		    <li class="list-group-item">
		      <b>Keterangan</b><br> <p><?= ucwords($data->datadiri_keterangan)?></p>
		    </li>			    	    
		  </ul>
		  <a href="<?= site_url('user/datadiri')?>" class="btn btn-primary btn-flat btn-block"><b>Edit</b></a>
		</div>
		<!-- /.box-body -->
		</div>		
	</div>
	<div class="col-sm-8">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title"><?= ucwords('kegiatan terakhir')?></h3>
			</div>
			<div class="box-body">
				<div class="table-responsive">
					<table class="table table-striped" width="100%">
						<thead>
							<tr>
								<th width="5%">No</th>
								<th width="20%">Tanggal</th>
								<th width="65%">Kegiatan</th>
								<th width="10%" class="text-center">Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php $i=1;foreach($kegiatan AS $row):?>
								<tr>
									<td><?=$i?></td>
									<td><?=date('d-m-Y',strtotime($row->kegiatan_tgl))?></td>
									<td><?=ucwords($row->kegiatan_judul)?></td>
									<td class="text-center">
										<a href="#" id="<?=$row->kegiatan_id?>" url="<?=base_url($global->url.'detail')?>" class="detail btn btn-xs btn-primary btn-flat"><span class="fa fa-eye"></span></a>
									</td>
								</tr>
							<?php $i++;endforeach;?>
						</tbody>
					</table>
				</div>				
			</div>
		</div>
	</div>
	</div>
<?php else:?>
	<div class="row">
		<div class="col-sm-12">
          <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Perharian!</h4>
            	Silahkan melengkapi data pribadi user
          </div>			
		</div>
	</div>
<?php endif;?>	
</div>
<!--DETAIL DATA-->
<div id="detail" class="modal fade">
</div>