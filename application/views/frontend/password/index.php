<div class="row">
	<div class="col-sm-6">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title"><?= ucwords($global->headline)?></h3>
			</div>
			<div class="box-body">
				<form method="POST" action="<?= base_url($global->url)?>">
					<div class="form-group">
						<label>Username</label>
						<input type="text" name="user_username" class="form-control" value="<?=$data->user_username?>">
					</div>
					<div class="form-group">
						<label>Password</label>
						<input type="password" name="user_password" class="form-control">
						<p class="help-block">biarkan kosong jika tidak ingin dirubah</p>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-block btn-flat btn-primary" name="submit" value="submit">Update</button>
					</div>					
				</form>
			</div>
		</div>
	</div>
</div>